package semver

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// Identifier Tests

func TestIdentifier_CompareNumbers(t *testing.T) {
	id1 := identifier("1234")
	id2 := identifier("5678")

	assert.Equal(t, id1.Compare(id2), 1)
	assert.Equal(t, id1.Compare(id1), 0)
	assert.Equal(t, id2.Compare(id2), 0)
	assert.Equal(t, id2.Compare(id1), -1)
}

func TestIdentifier_CompareMixed(t *testing.T) {
	id1 := identifier("1234")
	id2 := identifier("Nat4Number")

	assert.Equal(t, id1.Compare(id2), 1)
	assert.Equal(t, id1.Compare(id1), 0)
	assert.Equal(t, id2.Compare(id2), 0)
	assert.Equal(t, id2.Compare(id1), -1)
}

func TestIdentifier_CompareAlphanumeric(t *testing.T) {
	id1 := identifier("abc")
	id2 := identifier("def")

	assert.Equal(t, id1.Compare(id2), 1)
	assert.Equal(t, id1.Compare(id1), 0)
	assert.Equal(t, id2.Compare(id2), 0)
	assert.Equal(t, id2.Compare(id1), -1)
}

// Label Tests

func TestLabel_CompareNew(t *testing.T) {
	l, err := newLabel("abc.def")

	assert.Nil(t, err)
	assert.NotNil(t, l)
	assert.Equal(t, l.Value, "abc.def")
	assert.Equal(t, l.Identifiers, []identifier{"abc", "def"})
}

func TestLabel_CompareBasic(t *testing.T) {
	label1, _ := newLabel("abc")
	label2, _ := newLabel("d3f")

	assert.Equal(t, label1.Compare(label2), 1)
	assert.Equal(t, label1.Compare(label1), 0)
	assert.Equal(t, label2.Compare(label2), 0)
	assert.Equal(t, label2.Compare(label1), -1)
}

func TestLabel_CompareEmptyID(t *testing.T) {
	_, err := newLabel("123..abc")

	assert.NotNil(t, err)
}

func TestLabel_CompareOneEmpty(t *testing.T) {
	label1, _ := newLabel("")
	label2, _ := newLabel("d3f")

	assert.Equal(t, label1.Compare(label2), -1)
	assert.Equal(t, label1.Compare(label1), 0)
	assert.Equal(t, label2.Compare(label2), 0)
	assert.Equal(t, label2.Compare(label1), 1)
}

func TestLabel_CompareBothEmpty(t *testing.T) {
	label1, _ := newLabel("")
	label2, _ := newLabel("")

	assert.Equal(t, label1.Compare(label2), 0)
	assert.Equal(t, label1.Compare(label1), 0)
	assert.Equal(t, label2.Compare(label2), 0)
	assert.Equal(t, label2.Compare(label1), 0)
}

func TestLabel_CompareSameLen(t *testing.T) {
	label1, _ := newLabel("123.abc")
	label2, _ := newLabel("123.def")

	assert.Equal(t, label1.Compare(label2), 1)
	assert.Equal(t, label1.Compare(label1), 0)
	assert.Equal(t, label2.Compare(label2), 0)
	assert.Equal(t, label2.Compare(label1), -1)
}

func TestLabel_CompareDiffLen(t *testing.T) {
	label1, _ := newLabel("123.abc.x.y.z")
	label2, _ := newLabel("123.def")

	assert.Equal(t, label1.Compare(label2), 1)
	assert.Equal(t, label1.Compare(label1), 0)
	assert.Equal(t, label2.Compare(label2), 0)
	assert.Equal(t, label2.Compare(label1), -1)
}

func TestLabel_CompareWithSuffix(t *testing.T) {
	label1, _ := newLabel("123.abc")
	label2, _ := newLabel("123.abc.x.y.z")

	assert.Equal(t, label1.Compare(label2), 1)
	assert.Equal(t, label1.Compare(label1), 0)
	assert.Equal(t, label2.Compare(label2), 0)
	assert.Equal(t, label2.Compare(label1), -1)
}

func TestLabel_CompareString(t *testing.T) {
	label1, _ := newLabel("123.abc")

	assert.Equal(t, label1.String(), "123.abc")
}

// Version Tests

func TestVersion_New(t *testing.T) {
	version, err := New("3")
	assert.Nil(t, err)
	assert.NotNil(t, version)
	assert.Equal(t, version.Major, 3)
	assert.Equal(t, version.Minor, 0)
	assert.Equal(t, version.Patch, 0)
	assert.NotNil(t, version.Pre)
	assert.Equal(t, version.Pre.Value, "")
	assert.NotNil(t, version.Build)
	assert.Equal(t, version.Build.Value, "")

	version, err = New("3.2")
	assert.Nil(t, err)
	assert.NotNil(t, version)
	assert.Equal(t, version.Major, 3)
	assert.Equal(t, version.Minor, 2)
	assert.Equal(t, version.Patch, 0)
	assert.NotNil(t, version.Pre)
	assert.Equal(t, version.Pre.Value, "")
	assert.NotNil(t, version.Build)
	assert.Equal(t, version.Build.Value, "")

	version, err = New("3.2.1")
	assert.Nil(t, err)
	assert.NotNil(t, version)
	assert.Equal(t, version.Major, 3)
	assert.Equal(t, version.Minor, 2)
	assert.Equal(t, version.Patch, 1)
	assert.NotNil(t, version.Pre)
	assert.Equal(t, version.Pre.Value, "")
	assert.NotNil(t, version.Build)
	assert.Equal(t, version.Build.Value, "")

	version, err = New("3.2.1-get-set")
	assert.Nil(t, err)
	assert.NotNil(t, version)
	assert.Equal(t, version.Major, 3)
	assert.Equal(t, version.Minor, 2)
	assert.Equal(t, version.Patch, 1)
	assert.NotNil(t, version.Pre)
	assert.Equal(t, version.Pre.Value, "get-set")
	assert.NotNil(t, version.Build)
	assert.Equal(t, version.Build.Value, "")

	version, err = New("3.2.1-get-set+go")
	assert.Nil(t, err)
	assert.NotNil(t, version)
	assert.Equal(t, version.Major, 3)
	assert.Equal(t, version.Minor, 2)
	assert.Equal(t, version.Patch, 1)
	assert.NotNil(t, version.Pre)
	assert.Equal(t, version.Pre.Value, "get-set")
	assert.NotNil(t, version.Build)
	assert.Equal(t, version.Build.Value, "go")
}

func TestVersion_NewIncomplete(t *testing.T) {
	version, err := New("3+build")
	assert.Nil(t, err)
	assert.NotNil(t, version)
	assert.Equal(t, version.Major, 3)
	assert.Equal(t, version.Minor, 0)
	assert.Equal(t, version.Patch, 0)
	assert.NotNil(t, version.Pre)
	assert.Equal(t, version.Pre.Value, "")
	assert.NotNil(t, version.Build)
	assert.Equal(t, version.Build.Value, "build")
}

func TestVersion_NewBadVersion(t *testing.T) {
	_, err := New("")
	assert.NotNil(t, err)

	_, err = New("a")
	assert.NotNil(t, err)

	_, err = New(".")
	assert.NotNil(t, err)

	_, err = New("..-+")
	assert.NotNil(t, err)

	_, err = New("a")
	assert.NotNil(t, err)

	_, err = New("a.")
	assert.NotNil(t, err)

	_, err = New("3.a")
	assert.NotNil(t, err)

	_, err = New("3.2.a")
	assert.NotNil(t, err)

	_, err = New("-3.2.1")
	assert.NotNil(t, err)

	_, err = New("3.-2.1")
	assert.NotNil(t, err)

	_, err = New("3.2.-1")
	assert.NotNil(t, err)

	_, err = New("3.2.1-pre+one+two")
	assert.NotNil(t, err)

	_, err = New("3.2.1-bad..pre")
	assert.NotNil(t, err)

	_, err = New("3.2.1-pre+bad..build")
	assert.NotNil(t, err)
}

func TestVersion_GetBump(t *testing.T) {
	version1, _ := New("3.2.1-pre+build")
	version2, _ := New("3.2.1-pre+build")
	bump := version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartNone, 0})

	version1, _ = New("3.2.1-pre+build")
	version2, _ = New("3.2.1-pre+build-bump")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartBuild, 1})

	version1, _ = New("3.2.1-pre")
	version2, _ = New("3.2.1-pre+build")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartBuild, -1})

	version1, _ = New("3.2.1-pre+build")
	version2, _ = New("3.2.1-pre.bump")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartPre, 1})

	version1, _ = New("3.2.1")
	version2, _ = New("3.2.1-pre")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartPre, -1})

	version1, _ = New("3.2.1-pre+build")
	version2, _ = New("3.2.2")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartPatch, 1})

	version1, _ = New("3.2.3-pre+build")
	version2, _ = New("3.2.1")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartPatch, -2})

	version1, _ = New("3.2.1-pre+build")
	version2, _ = New("3.3")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartMinor, 1})

	version1, _ = New("3.4.1-pre+build")
	version2, _ = New("3.2.1")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartMinor, -2})

	version1, _ = New("3.2.1-pre+build")
	version2, _ = New("4")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartMajor, 1})

	version1, _ = New("5")
	version2, _ = New("3.2.1")
	bump = version1.GetBump(version2)
	assert.Equal(t, bump, Bump{PartMajor, -2})
}

func TestVersion_Compare(t *testing.T) {
	version1, _ := New("3.2.1-get.set+go")
	version2, _ := New("3.2.2")

	assert.Equal(t, version1.Compare(version2), 1)
	assert.Equal(t, version1.Compare(version1), 0)
	assert.Equal(t, version2.Compare(version2), 0)
	assert.Equal(t, version2.Compare(version1), -1)
}

func TestVersion_String(t *testing.T) {
	vString := "3.2.1-get.set+go"
	v, _ := New(vString)

	assert.Equal(t, vString, v.String())
}

func TestVersion_Bumps(t *testing.T) {
	v, _ := New("1.2.3-pre4+build5")

	assert.Equal(t, v.BumpMajor().String(), "2.0.0")
	assert.Equal(t, v.BumpMinor().String(), "1.3.0")
	assert.Equal(t, v.BumpPatch().String(), "1.2.4")

	bumpedPre, _ := v.BumpPre("new.pre")
	assert.Equal(t, bumpedPre.String(), "1.2.3-new.pre")
	_, err := v.BumpPre("bad..pre")
	assert.NotNil(t, err)

	bumpedBuild, _ := v.BumpBuild("new.build")
	assert.Equal(t, bumpedBuild.String(), "1.2.3-pre4+new.build")
	_, err = v.BumpBuild("bad..build")
	assert.NotNil(t, err)
}

func TestVersion_IsProd(t *testing.T) {
	notProd, _ := New("1.2.3-pre4+build5")
	assert.False(t, notProd.IsProd())

	notProd, _ = New("1.2.3-pre4")
	assert.False(t, notProd.IsProd())

	notProd, _ = New("1.2.3+build5")
	assert.False(t, notProd.IsProd())

	prod, _ := New("1.2.3")
	assert.True(t, prod.IsProd())
}

func newVersion(vs string) *Version {
	v, _ := New(vs)
	return v
}

func Test_Latest(t *testing.T) {
	versions := []*Version{
		newVersion("1.2.3"),
		newVersion("2.3.4"),
		newVersion("2.3.4-pre"),
	}

	latest := Latest(versions, false)
	assert.Equal(t, "2.3.4", latest.String())

	versions = []*Version{
		newVersion("1.2.3"),
		newVersion("2.3.4-pre2"),
		newVersion("2.3.4-pre1"),
	}

	latest = Latest(versions, false)
	assert.Equal(t, "2.3.4-pre2", latest.String())

	versions = []*Version{
		newVersion("1.2.3"),
		newVersion("2.3.4-pre2"),
		newVersion("2.3.4-pre1"),
		newVersion("1.2.2"),
	}

	latest = Latest(versions, true)
	assert.Equal(t, "1.2.3", latest.String())

	versions = []*Version{
		newVersion("1.2.3-pre0"),
		newVersion("2.3.4-pre2"),
		newVersion("2.3.4-pre1"),
	}

	latest = Latest(versions, true)
	assert.Equal(t, "0.0.0", latest.String())

	latest = Latest([]*Version{}, true)
	assert.Equal(t, "0.0.0", latest.String())
}

func Test_LatestString(t *testing.T) {
	versions := []string{
		"1.2.3",
		"40.70.badversion",
		"4.5.6",
	}

	latest := LatestString(versions, false)
	assert.Equal(t, "4.5.6", latest.String())
}
