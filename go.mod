module go.bagrat.io/semver

require (
	github.com/alecthomas/gometalinter v2.0.11+incompatible // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
)
