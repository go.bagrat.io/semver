package semver

import (
	"errors"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

// identifier is a string composed of alphanumeric characters and hypen.
//
// Values consisting of only digits are treated as numbers.
type identifier string

// Compare compares two identifiers according to SemVer rules.
func (id1 identifier) Compare(id2 identifier) int {
	idNum1, err1 := strconv.ParseUint(string(id1), 10, 64)
	idNum2, err2 := strconv.ParseUint(string(id2), 10, 64)

	switch {
	// Both are numbers...
	case err1 == nil && err2 == nil:
		// ...compare numerically
		switch {
		case idNum1 > idNum2:
			return -1
		case idNum1 == idNum2:
			return 0
		default:
			return 1
		}
	// id1 is a number and id2 is not...
	case err1 == nil && err2 != nil:
		// ...numeric value has lower precedence
		return 1
	// id1 is non-numeric and id2 is a number..
	case err1 != nil && err2 == nil:
		// ...non-number value has higher precedence
		return -1
	// Both are non-numeric
	default:
		// ...compare ASCII
		switch {
		case id1 > id2:
			return -1
		case id1 == id2:
			return 0
		default:
			return 1
		}
	}
}

// Label is a version label like pre-release version and build metadata.
type Label struct {
	Value       string
	Identifiers []identifier
}

// newLabel initializes a new label instance based on the supplied string value.
func newLabel(value string) (*Label, error) {
	idsString := strings.Split(value, ".")
	ids := make([]identifier, len(idsString))

	for i, id := range idsString {
		if len(idsString) > 1 && id == "" {
			return nil, errors.New("an identifier cannot be empty")
		}

		ids[i] = identifier(id)
	}

	return &Label{
		Value:       value,
		Identifiers: ids,
	}, nil
}

// Compare compares two version labels according to SemVer rules.
func (l *Label) Compare(other *Label) int {
	switch {
	case l.Value == "" && other.Value != "":
		return -1
	case l.Value == "" && other.Value == "":
		return 0
	case l.Value != "" && other.Value == "":
		return 1
	}

	for i, e := range l.Identifiers {
		if i == len(other.Identifiers) {
			return -1
		}

		if idResult := e.Compare(other.Identifiers[i]); idResult != 0 {
			return idResult
		}
	}

	if len(other.Identifiers) > len(l.Identifiers) {
		return 1
	}

	return 0
}

func (l *Label) String() string {
	return l.Value
}

// Part represent the Semantic Version part.
type Part int

const (
	// PartNone matched to none of the Semantic Version parts.
	PartNone Part = iota

	// PartMajor represents a major version path.
	PartMajor

	// PartMinor represents a minor version part.
	PartMinor

	// PartPatch represents a patch version part.
	PartPatch

	// PartPre represents a prebuild version part.
	PartPre

	// PartBuild represents a build version part.
	PartBuild
)

// Bump represents a change in a Version.
type Bump struct {
	Level Part
	Value int
}

// Version is a representation of a Semantic Version.
type Version struct {
	Major int
	Minor int
	Patch int
	Pre   *Label
	Build *Label
}

// New creates a new Version from the supplied string value.
func New(value string) (*Version, error) {
	partsBuild := strings.Split(value, "+")

	buildString := ""
	if len(partsBuild) > 2 {
		return nil, errors.New("too many build versions")
	}

	if len(partsBuild) == 2 {
		buildString = partsBuild[1]
	}

	build, err := newLabel(buildString)
	if err != nil {
		return nil, err
	}

	partsPre := strings.SplitN(partsBuild[0], "-", 2)

	preString := ""
	if len(partsPre) == 2 {
		preString = partsPre[1]
	}

	pre, err := newLabel(preString)
	if err != nil {
		return nil, err
	}

	parts := strings.Split(partsPre[0], ".")
	major, minor, patch := 0, 0, 0

	switch {
	case len(parts) == 3:
		patch, err = strconv.Atoi(parts[2])
		if err != nil || patch < 0 {
			return nil, errors.New("bad patch version")
		}
		fallthrough
	case len(parts) == 2:
		minor, err = strconv.Atoi(parts[1])
		if err != nil || minor < 0 {
			return nil, errors.New("bad minor version")
		}
		fallthrough
	case len(parts) == 1:
		major, err = strconv.Atoi(parts[0])
		if err != nil || major < 0 {
			return nil, errors.New("bad major version")
		}
	}

	return &Version{
		Major: major,
		Minor: minor,
		Patch: patch,
		Pre:   pre,
		Build: build,
	}, nil
}

// BumpMajor returns a new version with incremented major part.
func (v *Version) BumpMajor() *Version {
	return &Version{
		Major: v.Major + 1,
		Minor: 0,
		Patch: 0,
		Pre:   &Label{},
		Build: &Label{},
	}
}

// BumpMinor returns a new version with incremented minor part.
func (v *Version) BumpMinor() *Version {
	return &Version{
		Major: v.Major,
		Minor: v.Minor + 1,
		Patch: 0,
		Pre:   &Label{},
		Build: &Label{},
	}
}

// BumpPatch returns a new version with incremented patch part.
func (v *Version) BumpPatch() *Version {
	return &Version{
		Major: v.Major,
		Minor: v.Minor,
		Patch: v.Patch + 1,
		Pre:   &Label{},
		Build: &Label{},
	}
}

// BumpPre returns a new version by updating the prebuild part to supplied
// value.
func (v *Version) BumpPre(value string) (*Version, error) {
	pre, err := newLabel(value)
	if err != nil {
		return nil, err
	}

	return &Version{
		Major: v.Major,
		Minor: v.Minor,
		Patch: v.Patch,
		Pre:   pre,
		Build: &Label{},
	}, nil
}

// BumpBuild returns a new version by updating the build part with the supplied
// value.
func (v *Version) BumpBuild(value string) (*Version, error) {
	build, err := newLabel(value)
	if err != nil {
		return nil, err
	}

	return &Version{
		Major: v.Major,
		Minor: v.Minor,
		Patch: v.Patch,
		Pre:   v.Pre,
		Build: build,
	}, nil
}

// GetBump returns the Bump of the supplied version compared to the receiver.
func (v *Version) GetBump(o *Version) Bump {
	if v.Major != o.Major {
		return Bump{
			Level: PartMajor,
			Value: o.Major - v.Major,
		}
	}

	if v.Minor != o.Minor {
		return Bump{
			Level: PartMinor,
			Value: o.Minor - v.Minor,
		}
	}

	if v.Patch != o.Patch {
		return Bump{
			Level: PartPatch,
			Value: o.Patch - v.Patch,
		}
	}

	if c := v.Pre.Compare(o.Pre); c != 0 {
		return Bump{
			Level: PartPre,
			Value: c,
		}
	}

	if c := v.Build.Compare(o.Build); c != 0 {
		return Bump{
			Level: PartBuild,
			Value: c,
		}
	}

	return Bump{
		Level: PartNone,
		Value: 0,
	}
}

// IsProd determines whether the version is a production version or not.
func (v *Version) IsProd() bool {
	return v.Pre.Value == "" && v.Build.Value == ""
}

// Compare compares two Versions.
func (v *Version) Compare(o *Version) int {
	bump := v.GetBump(o)

	return bump.Value
}

// String return the string representation of a Version.
func (v *Version) String() string {
	vString := fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.Patch)

	if v.Pre.String() != "" {
		vString = fmt.Sprintf("%s-%s", vString, v.Pre.String())
	}

	if v.Build.String() != "" {
		vString = fmt.Sprintf("%s+%s", vString, v.Build.String())
	}

	return vString
}

// Latest returns the latest version from the provided list.
//
// If the prodOnly is true, the non-prod versions are filtered out first.
func Latest(versions []*Version, prodOnly bool) *Version {
	zero, _ := New("0.0.0")
	if len(versions) == 0 {
		return zero
	}

	versionsCopy := make([]*Version, len(versions))
	copy(versionsCopy, versions)

	sort.Slice(versionsCopy, func(i, j int) bool {
		if prodOnly && !versionsCopy[i].IsProd() {
			return true
		}

		if prodOnly && !versionsCopy[j].IsProd() {
			return false
		}

		res := versionsCopy[i].Compare(versionsCopy[j])

		return res == 1
	})

	latest := versionsCopy[len(versionsCopy)-1]
	if prodOnly && !latest.IsProd() {
		return zero
	}

	return latest
}

// LatestString is the same as Latest, but accepts a slice of strings.
//
// The bad versions are ignored.
func LatestString(versionsString []string, prodOnly bool) *Version {
	versions := make([]*Version, len(versionsString))
	for i, e := range versionsString {
		version, err := New(e)
		if err != nil {
			version, _ = New("0.0.0")
		}
		versions[i] = version
	}

	return Latest(versions, prodOnly)
}
