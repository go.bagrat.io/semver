# go options
TESTS := .
TESTFLAGS := -v -race -cover -coverprofile=coverage.out -short -run
GO111MODULE := on
export GO111MODULE

SHELL=/bin/sh


.PHONY: all
all: build check

.PHONY: check
check: lint coverage

.PHONY: lint
lint:
	gometalinter --tests --deadline=30s --disable-all \
	--enable=gofmt \
	--enable=misspell \
	--enable=deadcode \
	--enable=ineffassign \
	--enable=vet \
	--enable=golint \
	./...

.PHONY: test
test:
	go test $(TESTFLAGS) $(TESTS) ./...

.PHONY: coverage
coverage: test
	go tool cover -func=coverage.out

.PHONY: build
build:
	go build

.PHONY: fmt
fmt:
	go fmt $$(go list ./... | grep -v /vendor/)
	goimports -w $$(go list -f {{.Dir}} ./... | grep -v /vendor/)


HAS_GOMETALINTER := $(shell command -v gometalinter;)
HAS_GIT := $(shell command -v git;)

.PHONY: bootstrap
bootstrap:
ifndef HAS_GOMETALINTER
	GO111MODULE=off go get -u github.com/alecthomas/gometalinter
	GO111MODULE=off gometalinter --install
endif
ifndef HAS_GIT
	$(error git is required)
endif
